<?php
/**
 * Admin
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * GK_Admin Class
 */
class GK_Admin {

	public function __construct() {
		
		add_action( 'wp_loaded', array( $this, 'init' ) );
		
	}

	/**
	 * Hook in methods.
	 */
	public static function init() {

		add_action('admin_menu', array(__CLASS__, 'gplkit_create_menu'), 9999);
		
	}

	public static function gplkit_create_menu() {

		add_menu_page( 
	        __( 'GPL Kit Plugin Manager', 'gplkit' ),
	        'GPL Kit',
	        'manage_options',
	        'gplkit-plugin-manager',
	        array(__CLASS__, 'gplkit_settings_display'),
	        'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0My4zIDQ3LjgiPjxkZWZzPjxzdHlsZT4uY2xzLTF7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXNzZXQgMTwvdGl0bGU+PGcgaWQ9IkxheWVyXzIiIGRhdGEtbmFtZT0iTGF5ZXIgMiI+PGcgaWQ9IkxheWVyXzEtMiIgZGF0YS1uYW1lPSJMYXllciAxIj48cGF0aCBjbGFzcz0iY2xzLTEiIGQ9Ik0zOS44LDkuNCwyNS4xLjlhNy4wNiw3LjA2LDAsMCwwLTYuOSwwTDMuNSw5LjRhNi43Nyw2Ljc3LDAsMCwwLTMuNSw2djE3YTcsNywwLDAsMCwzLjUsNmwxNC43LDguNWE3LjA2LDcuMDYsMCwwLDAsNi45LDBsMTQuNy04LjVhNi43Nyw2Ljc3LDAsMCwwLDMuNS02di0xN0E3LDcsMCwwLDAsMzkuOCw5LjRaTTM0LjQsMjkuNkEyLjg1LDIuODUsMCwwLDEsMzMsMzIuMWwtOS45LDUuN2EzLDMsMCwwLDEtMi45LDBsLTkuOC01LjdBMi44NSwyLjg1LDAsMCwxLDksMjkuNlYxOC4yYTIuODUsMi44NSwwLDAsMSwxLjQtMi41TDIwLjMsMTBhMywzLDAsMCwxLDIuOSwwbDEwLjYsNi4xYS40Ny40NywwLDAsMSwwLC44bC00LjksMi45LTcuMS00LjEtNy4xLDQuMVYyOGw3LjEsNC4xTDI4LjksMjh2LS42SDIxLjhWMjMuN2EuNTguNTgsMCwwLDEsLjYtLjZIMzMuOWEuNTQuNTQsMCwwLDEsLjUuNVoiLz48L2c+PC9nPjwvc3ZnPg==',
	        59.1678
	    );
	    
	    add_submenu_page('gplkit-plugin-manager', 'GPL Kit Plugins', 'Plugins', 'manage_options', 'gplkit-plugin-manager' );

	    add_submenu_page('gplkit-plugin-manager', 'GPL Kit Themes', 'Themes', 'manage_options', 'gplkit-theme-manager',  array(__CLASS__, 'gplkit_themes_display'));

	}

	public static function gplkit_themes_display() {
		?>
		<div class="wrap">
		
		<h1>GPL Kit Themes <span class="beta-tag">BETA</span></h1>
		<div class="notice notice-info"><p>Let us know what you think about GPL Kit Themes! <a href="http://www.gplkit.com/contact/" target="_blank">Request a new theme to be added</a></p></div>

		<?php

			$installed_themes = wp_get_themes();
			if($gplkit_themes = get_option('gplkit_themes')) {
				$categories = array();
				foreach($gplkit_themes as $key => $theme) {
					$theme_categories = $theme['categories'];
					foreach($theme_categories as $slug => $category) {
						if (!in_array($category, $categories)) {
							$categories[$slug] = $category;
						}
					}
				}
			?>
			<div style="clear:both;"><input type="text" id="search_themes" name="search_themes" placeholder="Search Themes"></div>
			<ul class="subsubsub">
				<li><a class="gplfilter" data-filter="all">All</a></li>
				<?php foreach($categories as $slug => $category) {
					echo '<li> | <a class="gplfilter" data-filter=".'.$slug.'">'.$category.'</a></li>';
				} ?>
			</ul>

	        <form method="post" action="">

	            <ul id="Container" class="gkitcontainer">

	                <?php

	                	foreach($gplkit_themes as $key => $theme) {
	                		$theme_description = $theme['description'];
	                		$theme_category = $theme['category'];
							$maxLength = 200;
							$theme_author_name = $theme['author'];
							if (strlen($theme_description) > $maxLength) {
							    $stringCut = substr($theme_description, 0, $maxLength);
							    $theme_description = substr($stringCut, 0, strrpos($stringCut, ' ')); 
							}

							if (!empty($theme['gplkit_name'])) {
								$theme_name = $theme['gplkit_name'];
							} else {
								$theme_name = $theme['name'];
							}

							if ($theme['thumbnail']) {
                            	$thumbnail_bg = $theme['thumbnail'];
                            } else {
                            	$thumbnail_bg = 'https://www.gplkit.com/wp-content/uploads/2018/05/fallback.jpg';
                            }


                            ?>               		
							
						<li class="theme-block gkititem <?php echo implode(' ', (array)$theme_category); ?>" <?php if ( $theme['free'] == 1 ) { echo 'data-free="1"'; } else { echo 'data-free="2"'; } ?>">
	                        
	                        <div class="gk-theme-wrapper">
	                        	
	                        	<?php if ( in_array( $theme['name'], $installed_themes ) ) { ?>
									<div class="gk-notice-installed"><p>Installed</p></div>
								<?php } ?>

		                        <?php if ( get_option( 'gplkit_plugin_manager_activated' ) != 'Activated' && $theme['free'] !== 1 ) { ?>
									<div class="gk-notice-theme-notice"><p>Activate GPL Kit to install</p></div>
								<?php } ?>

								<?php if ( $theme['free'] == 1 && get_option( 'gplkit_plugin_manager_activated') != 'Activated' && !in_array( $theme['name'], $installed_themes ) ) { ?>
									<div class="ribbon is-green"><span class="ribbon__title">FREE</span></div>
								<?php } ?>

	                        	<div class="gk-theme-thumbnail" style="background-image: url('<?php echo $thumbnail_bg; ?>');"></div>
	                            
	                            <div class="gk-theme-meta">

		                           	<h3 class="gk-theme-title"><?php echo $theme_name; ?></h3>

		                            <div class="gk-theme-actions">

			                            <?php if ( $key == wp_get_theme() ) { ?>
		                            		<button type="submit" data-theme="<?php echo $key; ?>" class="button button-primary theme-install" value="Activated" disabled>Active</button>

		                            	<?php } else if ( in_array( $key, $installed_themes ) ) { ?>
											<button type="submit" data-theme="<?php echo $key; ?>" class="button button-primary theme-install" value="Install" disabled>Installed</button>	
		                            		
		                            	<?php } else if ( $theme['free'] == 1 && get_option( 'gplkit_plugin_manager_activated' ) != 'Activated' ) { ?>
		                            		<button type="submit" data-theme="<?php echo $key; ?>" class="button button-primary theme-install" value="Install">Install for free</button>
		                            		
		                            	<?php } else if ( get_option( 'gplkit_plugin_manager_activated' ) != 'Activated' ) { ?>
		                            		<button type="submit" id="install" class="button preview install-theme-preview" value="Install" disabled>Activate GPL Kit to install</button>

		                            	<?php } else { ?>
		                            		<button type="submit" data-theme="<?php echo $key; ?>" class="button button-primary theme-install" value="Install">Install</button>
		                            	<?php } ?>

	                            	</div>

	                            </div>	

	                        </div>

	                    </li>
                    <?php 
                		}
                	} ?>
                </ul>
                <div class="tablenav bottom">
					<div class="tablenav-pages">
						<div class="mixitup-page-list"></div>
						<div class="mixitup-page-stats"></div>
					</div>
				</div>

        	</form>
	    </div>

	    <script type="text/javascript">

	    	jQuery(document).ready(function($) {  
	        	$(".theme-install").click(function(e) {
	        		var installButton = jQuery(this);
	        		e.preventDefault();

	        		var data = {
						'action': 'gplkit_install_theme',
						'theme': $(this).attr('data-theme')
					};

					$(this).addClass('updating-message');
					$(this).parents('.theme-block').addClass('gk-focus');
					$(this).html('Installing...');
					
					jQuery.post(ajaxurl, data, function(response) {
						installButton.prop('disabled', true);
			   			installButton.text(response);
			   			if (response == 'Installed') {
			   				installButton.attr('disabled');
			   				installButton.removeClass('updating-message');
			   				installButton.parents('.gk-theme-wrapper').append( "<div class='gk-notice-installed'><p>Installed</p></div>" );
			   				installButton.parents('.gk-theme-wrapper').addClass('hide-ribbon');
			   			}
			   			if (response == 'Error 1003') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message');
			   				installButton.parents('.gk-theme-wrapper').append( "<div class='gk-notice-theme-error-notice'><p><strong>Duplicate theme name found</strong> Please uninstall duplicate theme to continue.</p></div>" );
			   				installButton.parents('.gk-theme-wrapper').addClass('hide-ribbon');
			   			}
			   			if (response == 'Error 1006') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message');
			   				installButton.parents('.gk-theme-wrapper').append( "<div class='gk-notice-theme-error-notice'><p><strong>GPL Kit not activated</strong> Please activate your GPL Kit licence to continue.</p></div>" );
			   				installButton.parents('.gk-theme-wrapper').addClass('hide-ribbon');
			   			}
			   			if (response == 'Error 1002') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message');
			   				installButton.parents('.gk-theme-wrapper').append( "<div class='gk-notice-theme-error-notice'><p><strong>Failed to download</strong> Could not connect to the GPL Kit repo.</p></div>" );
			   				installButton.parents('.gk-theme-wrapper').addClass('hide-ribbon');
			   			}
			   			if (response == 'Error 1008') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message');
			   				installButton.parents('.gk-theme-wrapper').append( "<div class='gk-notice-theme-error-notice'><p><strong>Unzip failed</strong> Unable to unzip the theme file.</p></div>" );
			   				installButton.parents('.gk-theme-wrapper').addClass('hide-ribbon');
			   			}
			   			if (response == 'Error 1001') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message');
			   				installButton.parents('.gk-theme-wrapper').append( "<div class='gk-notice-theme-error-notice'><p><strong>Copy failed</strong> Could not copy files to your theme folder.</p></div>" );
			   				installButton.parents('.gk-theme-wrapper').addClass('hide-ribbon');
			   			}

					});

	        	});

	        	var mixer = mixitup('.gkitcontainer', {
				    selectors: {
				        target: '.gkititem'
				    },
				    animation: {
				        duration: 300,
				        nudge: true,
				        reverseOut: false,
				        effects: 'fade translateZ(-100px)'
					},
					pagination: {
				        limit: 20,
				        maintainActivePage: false,
	        			hidePageListIfSinglePage: true
				    },
					load: {
						sort: 'free:asc'
					}
				});

	        	jQuery.expr[":"].contains = jQuery.expr.createPseudo(function(arg) {
				    return function( elem ) {
				        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
				    };
				});


				jQuery('#search_themes').keyup(function() {
					var search_term = jQuery(this).val();

					jQuery('.gkititem').each(function() {
						jQuery(this).attr('style','');
						if (jQuery(this).find('.gk-theme-title:contains("'+search_term+'")').length > 0) {
							jQuery(this).attr('style','display: inline-block;');
						} else {
							jQuery(this).attr('style','display: none;');
						}
					});
				});


	    	});
	   
		</script>  
		  	
		<?php
	}



	public static function gplkit_settings_display() { 
		
		if (!get_option('gplkit_plugins')) {
			GK_Updates::get_plugin_catalogue();
		}
	
	?>

	    <div class="wrap">

	    	<h1 class="wp-heading-inline">GPL Kit Plugins </h1>
	        <hr class="wp-header-end">
			
			<?php
				if($gplkit_plugins = get_option('gplkit_plugins')) {
					$categories = array();
					foreach($gplkit_plugins as $key => $plugin) {
						$plugin_categories = $plugin['categories'];
						foreach($plugin_categories as $slug => $category) {
							if (!in_array($category, $categories)) {
								$categories[$slug] = $category;
							}
						}
					}
			?>
			

			<div class="wp-filter">
				<div class="search-form search-plugins">
					<select class="select-filter">
		                <option value="all">All</option>
		                <?php foreach($categories as $slug => $category) {
							echo '<option value=".'.$slug.'">'.$category.'</option>';
						} ?>
		            </select>
					<input id="search_plugins" type="search" class="input wp-filter-search" data-ref="input-search" placeholder="Search plugins..."/>
	            </div>
			</div> <!-- .wp-filter -->

	        <form method="post" action="">
                   
                <ul id="container" class="gkitcontainer">

                	<?php

	                	foreach($gplkit_plugins as $key => $plugin) {
	                		$plugin_description = $plugin['description'];
	                		$plugin_category = $plugin['category'];
							$maxLength = 200;
							
							if (strlen($plugin_description) > $maxLength) {
							    $stringCut = substr($plugin_description, 0, $maxLength);
							    $plugin_description = substr($stringCut, 0, strrpos($stringCut, ' ')); 
							}

							if (!empty($plugin['gplkit_name'])) {
								$plugin_name = $plugin['gplkit_name'];
							} else {
								$plugin_name = $plugin['name'];
							}

	                	?>
	                		
	                        <li class="gkititem <?php echo implode(' ', (array)$plugin_category); ?>" <?php if ( $plugin['free'] == 1 && get_option( 'gplkit_plugin_manager_activated' ) != 'Activated' ) { echo 'data-free="1"'; } else { echo 'data-free="2"'; } ?>>
	                            <div class="gk-plugin-wrapper">
	                            	<span class="gk-plugin-title"><?php echo $plugin_name; ?></span>
	                                <div class="gk-plugin-inner">
	                                	<p><?php echo strip_tags($plugin_description, '<cite>'); ?></p>
	                                </div>
	                                
	                            		<?php if ( in_array( $key, apply_filters('active_plugins', get_option('active_plugins')) ) ) { ?>
	                            			
	                            			<div class="gk-notice-plugin-installed"><p>Active</p></div>	

										<?php } else if (file_exists(trailingslashit(WP_PLUGIN_DIR). $key) ) { ?>
											
											<div class="gk-notice-plugin-installed"><p>Installed</p></div>	
	                            		
	                            		<?php } else if ( $plugin['free'] == 1 && get_option( 'gplkit_plugin_manager_activated' ) != 'Activated' ) { ?>
	                            			
	                            			<div class="gk-plugin-meta">
	                            				<button type="submit" data-plugin="<?php echo $key; ?>" class="button button-primary plugin-install" value="Install">Install for free</button>
	                            			</div>	
	                            		
	                            		<?php } else if ( get_option( 'gplkit_plugin_manager_activated' ) != 'Activated' ) { ?>
	                            			
	                            			<div class="gk-notice-plugin-notice"><p>Activate GPL Kit to install</p></div>	

	                            		<?php } else { ?>
	                            			
	                            			<div class="gk-plugin-meta">
	                            				<button type="submit" data-plugin="<?php echo $key; ?>" class="button button-primary plugin-install" value="Install">Install</button>
	                            			</div>	

	                            		<?php } ?>
	                            	
	                            </div>
	                        </li>
                    <?php 
                		}
                	} ?>
                </ul>
                
                <div class="tablenav bottom">
					<div class="tablenav-pages">
						<div class="mixitup-page-list"></div>
						<div class="mixitup-page-stats"></div>
					</div>
				</div> <!-- .tablenav bottom -->

	        </form>

	    </div> <!-- .wrap -->

	    <script type="text/javascript">

	    jQuery(document).ready(function($) {

		    var selectFilter = document.querySelector('.select-filter');
	        var inputSearch = document.querySelector('input[type="search"]');
		    var mixer = mixitup('.gkitcontainer', {
			    selectors: {
			        target: '.gkititem'
			    },
			    animation: {
			        duration: 300,
			        nudge: false,
			        reverseOut: false,
			        effects: 'fade translateZ(-100px)'
				},
				pagination: {
			        limit: 50,
			        maintainActivePage: false,
	    			hidePageListIfSinglePage: true
			    },
			   	load: { 
			   		sort: 'free:asc' 
			   	},
				callbacks: {
	                onMixClick: function() {
	                    if (this.matches('.select-filter option')) {
	                        inputSearch.value = '';
	                    }
	                    jQuery('input#search_plugins').val('');
	                },
	            }
			});

	        selectFilter.addEventListener('change', function() {
	            var selector = selectFilter.value;
	            mixer.filter(selector);
	        });  
        
        	jQuery.expr[":"].contains = jQuery.expr.createPseudo(function(arg) {
			    return function( elem ) {
			        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
			    };
			});

			jQuery('input#search_plugins').keyup(function() {
				var search_term = jQuery(this).val();
				jQuery('.gkititem').each(function() {
					jQuery(this).attr('style','');
					if (jQuery(this).find('.gk-plugin-title:contains("'+search_term+'")').length > 0) {
						jQuery(this).attr('style','display: inline-block;');
					} else {
						jQuery(this).attr('style','display: none;');
					}
				});

				if( !jQuery(this).val() ) {
			        mixer.filter('.all');
			    }

			});

        	// Install Plugin
        	$(".plugin-install").click(function(e) {
        		var installButton = jQuery(this);
        		e.preventDefault();

        		var data = {
					'action': 'gplkit_install_plugin',
					'plugin': $(this).attr('data-plugin')
				};
				
				$(this).addClass('updating-message');
				$(this).html('Installing...');

				jQuery.post(ajaxurl, data, function(response) {
						installButton.prop('disabled', true);
			   			installButton.text(response);
			   			if (response == 'Installed') {
			   				installButton.attr('disabled');
			   				installButton.removeClass('updating-message button-primary');
			   				installButton.parents('.gk-plugin-wrapper').append( "<div class='gk-notice-plugin-installed'><p>Installed</p></div>" );
			   			}
			   			if (response == 'Error 1001') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message button-primary');
			   				installButton.parents('.gk-plugin-wrapper').append( "<div class='gk-notice-plugin-notice-error'><p>Error - Please try again</p></div>" );
			   			}
			   			if (response == 'Error 1002') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message button-primary');
			   				installButton.parents('.gk-plugin-wrapper').append( "<div class='gk-notice-plugin-notice-error'><p>Error connecting to GPL Kit</p></div>" );
			   			}
			   			if (response == 'Error 1003') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message button-primary');
			   				installButton.parents('.gk-plugin-wrapper').append( "<div class='gk-notice-plugin-notice-error'><p>Plugin already installed</p></div>" );
			   			}
			   			if (response == 'Error 1006') {
			   				installButton.text('Error');
			   				installButton.prop('disabled', true);
			   				installButton.removeClass('updating-message button-primary');
			   				installButton.parents('.gk-plugin-wrapper').append( "<div class='gk-notice-plugin-notice'><p>Activate GPL Kit to install</p></div>" );
			   			}
					
				     
				});

        	});

    	});
	    </script>

	<?php }	
}

GK_Admin::init();